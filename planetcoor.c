// standard library

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>


// program is based on the algorithm described here : http://www.stjarnhimlen.se/comp/tutorial.html

#define PI 3.14159265358979323846
#define RADEG (180.0/PI)
#define DEGRAD (PI/180.0)
#define sind(x) sin((x)*DEGRAD)
#define cosd(x) cos((x)*DEGRAD)
#define tand(x) tan((x)*DEGRAD)
#define asind(x) (RADEG*asin(x))
#define acosd(x) (RADEG*acos(x))
#define atand(x) (RADEG*atan(x))
#define atan2d(y,x) (RADEG*atan2((y),(x)))

#define MERCURY 0
#define VENUS 1
#define MARS 2
#define JUPITER 3
#define SATURN 4
#define URANUS 5
#define NEPTUNE 6

#define MERCURYAPPRENTDIA 6.74
#define VENUSAPPRENTDIA 6.74
#define MERCURYAPPRENTDIA 6.74


double rev( double x )
{
	return x - floor(x/360.0)*360.0;
}


double cbrt( double x )
{
	if ( x > 0.0 )
		return exp( log(x) / 3.0 );
	else if ( x < 0.0 )
		return -cbrt(-x);
	else /* x == 0.0 */
		return 0.0;
}

void sph2xyz(double * radecIn, double * xyzOut){
	//input: radecIn, an array like [RA, DEC, r]
	//output: xyzOut, an array like [x, y, z]
	xyzOut[0] = radecIn[2] * cosd(radecIn[0]) * cosd(radecIn[1]);
	xyzOut[1] = radecIn[2] * sind(radecIn[0]) * cosd(radecIn[1]);
	xyzOut[2] = radecIn[2] * sind(radecIn[1]);
	return;
}

void xyz2sph(double * xyzIn, double * radecOut){
	radecOut[2] = sqrt(xyzIn[0]*xyzIn[0] + xyzIn[1]*xyzIn[1] + xyzIn[2]*xyzIn[2]);
	radecOut[0] = rev(atan2d(xyzIn[1], xyzIn[0]));
	radecOut[1] = atan2d(xyzIn[2], sqrt(xyzIn[0]*xyzIn[0]+ xyzIn[1]*xyzIn[1]));
	return;
}

void eclip2equat(double * eclipIn, double * equatOut, double obliquity){
	//obliquity is defined as in degree, approx. 23.4
	equatOut[0] = eclipIn[0];
	equatOut[1] = eclipIn[1] * cosd(obliquity) - eclipIn[2] * sind(obliquity);
	equatOut[2] = eclipIn[1] * sind(obliquity) + eclipIn[2] * cosd(obliquity);
	return;
}

void equat2eclip(double * equatIn, double * eclipOut, double obliquity){
	//obliquity is defined as in degree, approx. 23.4
	eclipOut[0] = equatIn[0];
	eclipOut[1] = equatIn[1] * cosd(-1*obliquity) - equatIn[2] * sind(-1*obliquity);
	eclipOut[2] = equatIn[1] * sind(-1*obliquity) + equatIn[2] * cosd(-1*obliquity);
	return;
}

double daysfromJD(int year, int month, int date, double ut){
	//formula valid from 1900 to 2100
	int days = 0;
	days = 367*year - (7*(year+((month+9)/12)))/4 + (275*month)/9 + date - 730530;
	return (double)days+ut/24;
}


double sunMeanLogitude(double d){
	double omega = rev(282.9404 + 4.70935e-5 * d); //longitude of perihelion in degree
	double M = rev(356.0470 + 0.985600285 * d); //mean anomaly
	double L = rev(omega + M);
	return L;
}

//calculate sidereal time
double siderealTime(double d, double UT, double LON){
	//here we assume d doesn't account UT time, so is 00:00 right now
	//d is day from julian date, use daysfromJD to calculate, UT is the ut time in decimal, LON is longitude in degree
	//output is in hour
	double result = 0;
	double GMST0 = (rev(sunMeanLogitude(d)+180))/15; //in hour, sidereal time at greenwich meridian at 00:00 right now
	result = GMST0 + UT + LON/15;
	if(result<0){
		result = result+24;
	}
	return result;
}

//calculate hour angle
double hourAngel(double sidTime, double RAhour){
	// sidTime and RA must be in the same unit here we assume RA is in unit of hour
	return sidTime - RAhour;
}


void radec2azialt(double LAT, double HA, double * radec, double * aziAlt){
	double xyz[3];
	xyz[0] = cosd(HA) * cosd(radec[1]);
	xyz[1] = sind(HA) * cosd(radec[1]);
	xyz[2] = sind(radec[1]);
	double xyzhor[3];
	xyzhor[0] = xyz[0] * cosd(90-LAT) - xyz[2]*sind(90-LAT);
	xyzhor[1] = xyz[1];
	xyzhor[2] = xyz[0] * sind(90-LAT) + xyz[2]*cosd(90-LAT);

	aziAlt[0] = atan2d(xyzhor[1], xyzhor[0]) + 180; //azimuth
	aziAlt[1] = atan2d(xyzhor[2], sqrt(xyzhor[0]*xyzhor[0] + xyzhor[1]*xyzhor[1])); //altitutde
	return;
}

// calculate sun's position in ra and dec
void sunRADEC(double d, double UT, double LAT, double LON, double * radec, double * aziAlt){
	//calculate sun's position for date,
	//input: date (number of days from JD, use daysfromJD function to calculate, not account UT, so 00:00 ut time right now) and an empty array
	//input, UT time in decimal, LON, longitude in degree
	//output: radec array will be filled with, RA, DEC (in degree), and radius, aziAlt is a 2d array, filled with azimuth angle and altitude (in degree)
	double omega = rev(282.9404 + 4.70935e-5 * d); //longitude of perihelion in degree
	double e = 0.016709 - 1.151e-9 * d; //eccentricity
	double M = rev(356.0470 + 0.985600285 * d); //mean anomaly
	double E = rev(M + (180/PI) * e * sind(M) * (1 + e*cosd(M))); //auxilliarry angle
	double x = cosd(E) - e; //calculate sun's rectangular coor in ecliptic plane
	double y = sind(E) * sqrt(1-e*e);
	double r = sqrt(x*x + y*y); //distance and true anomaly
	double v = atan2d(y, x);
	double lon = rev(v + omega);
	double recEclipCoo[3];
	recEclipCoo[0] = r * cosd(lon); //calculate rectangular coordinates, rotate to equatorial coordinates and compute RA and DEC
	recEclipCoo[1] = r * sind(lon);
	recEclipCoo[2] = 0;
	double recEquatCoo[3];
	eclip2equat(recEclipCoo, recEquatCoo, 23.4406); //rotate to equatorial coordinates
	xyz2sph(recEquatCoo, radec);
	double sidTime = siderealTime(d, UT, LON);
	double hrangle = rev(hourAngel(sidTime, radec[0]/15) * 15); //hour angle in degree
	//coordinate system transformation
	radec2azialt(LAT, hrangle, radec, aziAlt); //convert to altitude and azimuth
	return;
}

//calculate moon's position in ra and dec
void moonRADEC(double d, double UT, double LAT, double LON, unsigned int parallaxCorrect, double * radec, double * aziAlt, double * auxinfo){
	//given days from JD, UT time, latitutde, longitude, return ra dec and altitude azimuth, parralax is corrected if parallaxCorrect is 1
	//output: returns the topocentric cooridnate of moon now
	double N = rev(125.1228 - 0.0529538083*d); //in degree, longitudinal ascending node
	double i = 5.1454; //inclination of the orbit
	double omega = rev(318.0634 + 0.1643573223*d); //in degree, argument of perigee
	double a = 60.2666; //mean distance, in unit of ???
	double e = 0.054900; //eccentricity
	double M = rev(115.3654 + 13.0649929509*d); //mean anomaly

	double eTolerance = 0.005; //tolerances for approximation of eccentricity of the moon's orbit. E will be updated with each iteration
	double E0 = rev(M + (180/PI) * e * sind(M) * (1 + e * cosd(M)));
	double E1 = 0;
	while(fabs(E1 - E0)>eTolerance){
		E0 = E1;
		E1 = rev(E0 - (E0 - (180/PI) * e * sind(E0) - M) / (1 - e * cosd(E0)));
	}
	double E = E1;

	//calculate moon's position in plane of the lunar orbit, xy coordinate
	double xy[2];
	xy[0] = a * (cosd(E) - e);
	xy[1] = a * sqrt(1 - e*e) * sind(E);
	//r v coordinate
	double rv[2];
	rv[0] = sqrt(xy[0]*xy[0] + xy[1]*xy[1]);
	rv[1] = rev(atan2d(xy[1],xy[0]));

	double xyzEclip[3];
	xyzEclip[0] = rv[0] * (cosd(N) * cosd(rv[1]+omega) - sind(N) * sind(rv[1]+omega) * cosd(i));
	xyzEclip[1] = rv[0] * (sind(N) * cosd(rv[1]+omega) + cosd(N) * sind(rv[1]+omega) * cosd(i));
	xyzEclip[2] = rv[0] * sind(rv[1] + omega) * sind(i);

	double lonLatDis[3]; // longitude, latitutde, distance
	lonLatDis[0] = rev(atan2d(xyzEclip[1], xyzEclip[0]));
	lonLatDis[1] = (atan2d(xyzEclip[2], sqrt(xyzEclip[0] * xyzEclip[0] + xyzEclip[1] * xyzEclip[1])));
	lonLatDis[2] = sqrt(xyzEclip[0] * xyzEclip[0] + xyzEclip[1] * xyzEclip[1] + xyzEclip[2] * xyzEclip[2]);
	
	double lonSun = sunMeanLogitude(d);
	double lonMoon = rev(N + omega + M);
	double Msun = rev(356.0470 + 0.985600285 * d); 
	double D = lonMoon - lonSun; //mean elongation
	double F = lonMoon - N; //argument of latitutde

	//21 terms of perturbations
	double perturbationLon[12];
	double perturbationLat[5];
	double perturbationDist[3];

	perturbationLon[0] = -1.274 * sind(M - 2 * D);
	perturbationLon[1] =  0.658 * sind(2 * D);
	perturbationLon[2] = -0.186 * sind(Msun);
	perturbationLon[3] = -0.059 * sind(2 * M - 2 * D);
	perturbationLon[4] = -0.057 * sind(M - 2 * D + Msun);
	perturbationLon[5] =  0.053 * sind(M + 2 * D);
	perturbationLon[6] =  0.046 * sind(2 * D - Msun);
	perturbationLon[7] =  0.041 * sind(M - Msun);
	perturbationLon[8] = -0.035 * sind(D);
	perturbationLon[9] = -0.031 * sind(M + Msun);
	perturbationLon[10]= -0.015 * sind(2 * F - 2 * D);
	perturbationLon[11]=  0.011 * sind(M - 4 * D);

	double perturbationLonTotal = 0;
	for(int i=0;i<12;i++){
		perturbationLonTotal += perturbationLon[i];
	}

	perturbationLat[0] = -0.173 * sind(F - 2 * D);
	perturbationLat[1] = -0.055 * sind(M - F - 2 * D);
	perturbationLat[2] = -0.046 * sind(M + F - 2 * D);
	perturbationLat[3] =  0.033 * sind(F + 2 * D);
	perturbationLat[4] =  0.017 * sind(2 * M + F);

	double perturbationLatTotal = 0;
	for(int i=0;i<5;i++){
		perturbationLatTotal += perturbationLat[i];
	}

	perturbationDist[0] = -0.58 * cosd(M - 2 * D);
	perturbationDist[1] = -0.46 * cosd(2 * D);

	double perturbationDistTotal = 0;
	for(int i=0;i<2;i++){
		perturbationDistTotal += perturbationDist[i];
	}

	lonLatDis[0] += perturbationLonTotal;
	lonLatDis[1] += perturbationLatTotal;
	lonLatDis[2] += perturbationDistTotal;

	double oblecl = 23.4393 - 3.563e-7 * d;
	double xeclip = lonLatDis[2] * cosd(lonLatDis[0]) * cosd(lonLatDis[1]);
	double yeclip = lonLatDis[2] * sind(lonLatDis[0]) * cosd(lonLatDis[1]);
	double zeclip = lonLatDis[2] * sind(lonLatDis[1]);

	double xequat = xeclip;
	double yequat = yeclip * cosd(oblecl) - zeclip * sind(oblecl);
	double zequat = yeclip * sind(oblecl) + zeclip * cosd(oblecl);

	radec[0] = rev(atan2d(yequat, xequat));
	radec[1] = atan2d(zequat, sqrt(xequat*xequat + yequat*yequat));

	if(parallaxCorrect==0){
		double LST = siderealTime(d, UT, LON);
		double HA = rev(LST * 15 - radec[0]);
		radec2azialt(LAT, HA, radec, aziAlt);
		return;
	}
		
	//convert from geocentric to topocentric, this correction is only needed for moon for position accuracy of half a arcminunte, other planets and sun are far enough for the accuracy without it
	double gcLAT = LAT - 0.1924 * sind(2 * LAT);
	double rho = 0.99833 + 0.00167 * cosd(2 * LAT);

	double LST = siderealTime(d, UT, LON);
	double HA = rev(LST * 15 - radec[0]);

	double mpar = asind(1/lonLatDis[2]);

	double g = atand(tand(gcLAT)/cosd(HA)); //auxillary angle
	double topRA = rev(radec[0] - mpar * rho * cosd(gcLAT) * sind(HA) / cosd(radec[1]));
	double topDEC = radec[1] - mpar * rho * sind(gcLAT) * sind(g - radec[1]) / sind(g);

	radec[0] = topRA;
	radec[1] = topDEC;

	radec2azialt(gcLAT, HA, radec, aziAlt);


	//calculate distance, planet, sun, earth, elongation, phase angle, phase value
	double slon = lonSun;
	double mlon = lonLatDis[0];
	double mlat = lonLatDis[1];

	double elong = acosd(cosd(slon - mlon) * cosd(mlat));
	double FV = 180 - elong;
	double phase = (1+cosd(FV))/2;

	auxinfo[0] = elong;
	auxinfo[1] = FV;
	auxinfo[2] = phase;

	return;
}



void planetRADEC(unsigned int planet, double d, double UT, double LAT, double LON, double * radec, double * aziAlt, double * auxInfoPlanet){
	double N; //longitude of ascending node
	double i; //inclination
	double omega; //argument of perihelion
	double a; //semi-major axis
	double e; //eccentricity
	double M; //mean anomaly

	switch(planet){
		case MERCURY:
			N = rev(48.3313 + 3.24587e-5 * d);
			i = 7.0047  + 5.00e-8 * d;
			omega = rev(29.1241 + 1.01444e-5 * d);
			a = 0.387098;
			e = 0.205635 + 5.59e-10 * d;
			M = rev(168.6562 + 4.0923344368 * d);
			break;
		case VENUS:
			N = rev(76.6799 + 2.46590e-5 * d);
			i = 3.3946 + 2.75e-8 * d;
			omega = rev(54.8910 + 1.38374e-5 * d);
			a = 0.72333;
			e = 0.006773 - 1.302e-9 * d;
			M = rev(48.0052 + 1.6021302244 * d);
			break;
		case MARS:
			N = rev(49.5574 + 2.11081e-5 * d);
			i = 1.8497  - 1.78e-8 * d;
			omega = rev(286.5016 + 2.92961e-5 * d);
			a = 1.523688;
			e = 0.093405 + 2.516e-9 * d;
			M = rev(18.6021 + 0.5240207766 * d);
			break;
		case JUPITER:
			N = rev(100.4542 + 2.76854e-5 * d);
			i = 1.3030  - 1.557e-7 * d;
			omega = rev(273.8777 + 1.64505e-5 * d);
			a = 5.20256;
			e = 0.048498 + 4.469e-9 * d;
			M = rev(19.8950 + 0.0830853001 * d);
			break;
		case SATURN:
			N = rev(113.6634 + 2.38980e-5 * d);
			i = 2.4886  - 1.081e-7 * d;
			omega = rev(339.3939 + 2.97661e-5 * d);
			a = 9.55475;
			e = 0.055546 - 9.499e-9 * d;
			M = rev(316.9670 + 0.0334442282 * d);
			break;
		case URANUS:
			N = rev(74.0005 + 1.3978e-5 * d);
			i = 0.7733  + 1.9e-8 * d;
			omega = rev(96.6612 + 3.0565e-5 * d);
			a = 19.18171 - 1.55e-8 * d;
			e = 0.047318 + 7.45e-9 * d;
			M = rev(142.5905 + 0.011725806 * d);
			break;
		case NEPTUNE:
			N = rev(131.7806 + 3.0173e-5 * d);
			i = 1.7700  - 2.55e-7 * d;
			omega = rev(272.8461 - 6.027e-6 * d);
			a = 30.05826 + 3.313e-8 * d;
			e = 0.008606 + 2.15e-9 * d;
			M = rev(260.2471 + 0.005995147 * d);
			break;
	}
	//calculate eccentricity 	

	double eTolerance = 0.005; //tolerances for approximation of eccentricity of the planet's orbit. E will be updated with each iteration
	double E0 = rev(M + (180/PI) * e * sind(M) * (1 + e * cosd(M)));
	double E1 = 0;
	while(fabs(E1 - E0)>eTolerance){
		E0 = E1;
		E1 = rev(E0 - (E0 - (180/PI) * e * sind(E0) - M) / (1 - e * cosd(E0)));
	}
	double E = E1;
	//calculate planet's position in plane of the planet orbit, xy coordinate
	double xy[2];
	xy[0] = a * (cosd(E) - e);
	xy[1] = a * sqrt(1 - e*e) * sind(E);
	//r v coordinate
	double rv[2];
	rv[0] = sqrt(xy[0]*xy[0] + xy[1]*xy[1]);
	rv[1] = rev(atan2d(xy[1],xy[0]));
	//calculate heliocentric ecliptic rectangular coordinates
	double xyzEclip[3];
	xyzEclip[0] = rv[0] * (cosd(N) * cosd(rv[1]+omega) - sind(N) * sind(rv[1]+omega) * cosd(i));
	xyzEclip[1] = rv[0] * (sind(N) * cosd(rv[1]+omega) + cosd(N) * sind(rv[1]+omega) * cosd(i));
	xyzEclip[2] = rv[0] * sind(rv[1] + omega) * sind(i);

	double lonLatDis[3]; // longitude, latitutde, distance
	lonLatDis[0] = rev(atan2d(xyzEclip[1], xyzEclip[0]));
	lonLatDis[1] = (atan2d(xyzEclip[2], sqrt(xyzEclip[0] * xyzEclip[0] + xyzEclip[1] * xyzEclip[1])));
	lonLatDis[2] = sqrt(xyzEclip[0] * xyzEclip[0] + xyzEclip[1] * xyzEclip[1] + xyzEclip[2] * xyzEclip[2]);
	
	
	//Jupiter, Saturn and Uranus need to correct perturbations in LON and LAT
	//Jupiter
	if(planet==JUPITER){
		double MjLon[7];
		//Jupiter Saturn perturbation
		double Mj = rev(19.8950 + 0.0830853001 * d);
		double Ms = rev(316.9670 + 0.0334442282 * d);
		MjLon[0] = -0.332 * sind(2 * Mj - 5 * Ms - 67.6);
		MjLon[1] = -0.056 * sind(2 * Mj - 2 * Ms + 21);
		MjLon[2] = +0.042 * sind(3 * Mj - 5 * Ms + 21);
		MjLon[3] = -0.036 * sind(Mj - 2 * Ms);
		MjLon[4] = +0.022 * cosd(Mj - Ms);
		MjLon[5] = +0.023 * sind(2 * Mj - 3 * Ms + 52);
		MjLon[6] = -0.016 * sind(Mj - 5 * Ms - 69);
		double MjLonTotal = 0;
		for(int i=0;i<7;i++){
			MjLonTotal +=MjLon[i];
		}
		lonLatDis[0] += MjLonTotal; //add in the perturbation term
	}else if(planet==SATURN){
		double MsLon[5];
		double MsLat[2];
		double Mj = rev(19.8950 + 0.0830853001 * d);
		double Ms = rev(316.9670 + 0.0334442282 * d);
		MsLon[0] = +0.812 * sind(2 * Mj - 5 * Ms - 67.6);
		MsLon[1] = -0.229 * cosd(2 * Mj - 4 * Ms - 2);
		MsLon[2] = +0.119 * sind(Mj - 2 * Ms - 3);
		MsLon[3] = +0.046 * sind(2 * Mj - 6 * Ms - 69);
		MsLon[4] = +0.014 * sind(Mj - 3 * Ms + 32);

		MsLat[0] = -0.02 * cosd(2 * Mj - 4 * Ms - 2);
		MsLat[1] = +0.018* sind(2 * Mj - 6 * Ms - 49);
		double MsLonTotal = 0;
		double MsLatTotal = 0;
		for(int i=0;i<5;i++){
			MsLonTotal +=MsLon[i];
		}
		for(int i=0;i<2;i++){
			MsLatTotal +=MsLat[i];
		}
		
		lonLatDis[0] += MsLonTotal; //add in the perturbation term
		lonLatDis[1] += MsLatTotal;
	}else if(planet==URANUS){
		double MuLon[3];
		double Mj = rev(19.8950 + 0.0830853001 * d);
		double Ms = rev(316.9670 + 0.0334442282 * d);	
		double Mu = rev(142.5905 + 0.011725806 * d);
		MuLon[0] = +0.04 * sind(Ms - 2 * Mu + 6);
		MuLon[1] = +0.035* sind(Ms - 3 * Mu + 33);
		MuLon[2] = -0.015* sind(Mj - Mu + 20);
		double MuLonTotal = 0;
		for(int i=0;i<3;i++){
			MuLonTotal +=MuLon[i];
		}
		lonLatDis[0] += MuLonTotal; //add in the perturbation term
	}

	//ecliptical system to equatorial system
	double oblecl = 23.4393 - 3.563e-7 * d;
	double xeclip = lonLatDis[2] * cosd(lonLatDis[0]) * cosd(lonLatDis[1]);
	double yeclip = lonLatDis[2] * sind(lonLatDis[0]) * cosd(lonLatDis[1]);
	double zeclip = lonLatDis[2] * sind(lonLatDis[1]);

	double omegaSun = rev(282.9404 + 4.70935e-5 * d); //longitude of perihelion in degree
	double eSun = 0.016709 - 1.151e-9 * d; //eccentricity
	double MSun = rev(356.0470 + 0.985600285 * d); //mean anomaly
	double ESun = rev(MSun + (180/PI) * eSun * sind(MSun) * (1 + eSun * cosd(MSun))); //auxilliarry angle
	double xSun = cosd(ESun) - eSun; //calculate sun's rectangular coor in ecliptic plane
	double ySun = sind(ESun) * sqrt(1-eSun * eSun);
	double rSun = sqrt(xSun * xSun + ySun * ySun); //distance and true anomaly
	double vSun = atan2d(ySun, xSun);
	double lonSun = rev(vSun + omegaSun);
	double recEclipCooSun[3];
	recEclipCooSun[0] = rSun * cosd(lonSun); //calculate rectangular coordinates, rotate to equatorial coordinates and compute RA and DEC
	recEclipCooSun[1] = rSun * sind(lonSun);
	recEclipCooSun[2] = 0;

	double xyzGeoc[3];
	xyzGeoc[0] = xeclip + recEclipCooSun[0];
	xyzGeoc[1] = yeclip + recEclipCooSun[1];
	xyzGeoc[2] = zeclip + recEclipCooSun[2];

	double xyzEquat[3];
	xyzEquat[0] = xyzGeoc[0];
	xyzEquat[1] = xyzGeoc[1] * cosd(oblecl) - xyzGeoc[2] * sind(oblecl);
	xyzEquat[2] = xyzGeoc[1] * sind(oblecl) + xyzGeoc[2] * cosd(oblecl);

	xyz2sph(xyzEquat, radec);

	//calculate distance, planet, sun, earth, elongation, phase angle, phase value
	double LST = siderealTime(d, UT, LON);
	double HA = rev(LST * 15 - radec[0]);
	radec2azialt(LAT, HA, radec, aziAlt);

	double r = sqrt(xyzEclip[0]*xyzEclip[0]+xyzEclip[1]*xyzEclip[1]+xyzEclip[2]*xyzEclip[2]);
	double R = sqrt(xyzEquat[0]*xyzEquat[0]+ xyzEquat[1]*xyzEquat[1] + xyzEquat[2]*xyzEquat[2]);
	double s = rSun;

	double elong = acosd((s*s + R*R - r*r)/(2*s*R));
	double FV = acosd((r*r + R*R - s*s)/(2*r*R));
	double phase = (1+cosd(FV))/2;

	auxInfoPlanet[0] = R;
	auxInfoPlanet[1] = r;
	auxInfoPlanet[2] = elong;
	auxInfoPlanet[3] = FV;
	auxInfoPlanet[4] = phase;

	return;
}




int main(int argc, char ** argv){

	int year = 2019;
	int month = 12;
	int date = 27;
	double UT = 0;

	double lat = 39.9042;
	double lon = 116.4074;
	unsigned int correctParallaxFlag = 1;

	double radecSunCoo[3];
	double azalSunCoo[2];

	double radecMoonCoo[2];
	double azalMoonCoo[2];

	double radecPlaCoo[2];
	double azalPlaCoo[2];

	double auxInfoMoon[3];
	double auxInfoPlanet[5];

	sunRADEC(daysfromJD(year, month, date, UT), UT, lat, lon, radecSunCoo, azalSunCoo);
	printf("sun: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecSunCoo[0], radecSunCoo[1], azalSunCoo[0], azalSunCoo[1]);
	printf("\n");


	moonRADEC(daysfromJD(year, month, date, UT), UT, lat, lon, correctParallaxFlag, radecMoonCoo, azalMoonCoo, auxInfoMoon);
	printf("moon: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecMoonCoo[0], radecMoonCoo[1], azalMoonCoo[0], azalMoonCoo[1]);
	printf("elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoMoon[0], auxInfoMoon[1], auxInfoMoon[2]);
	printf("\n");


	planetRADEC(MERCURY, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("mercury: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");


	planetRADEC(VENUS, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("venus: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");


	planetRADEC(MARS, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("mars: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");


	planetRADEC(JUPITER, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("jupiter: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");


	planetRADEC(SATURN, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("saturn: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");


	planetRADEC(URANUS, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("uranus: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");


	planetRADEC(NEPTUNE, daysfromJD(year, month, date, UT), UT, lat, lon, radecPlaCoo, azalPlaCoo, auxInfoPlanet);
	printf("neptune: RA: %f deg, DEC: %f deg, Azi (0 N-E-S-W 360): %f deg, Alt (0 ground-zenith 90): %f deg\n", radecPlaCoo[0], radecPlaCoo[1], azalPlaCoo[0], azalPlaCoo[1]);
	printf("R (planet - sun): %f a.u., r (planet - earth): %f a.u., elong: %f deg, FV (full 0 - 180 new): %f deg , phase (new 0-1 full): %f \n", auxInfoPlanet[0], auxInfoPlanet[1], auxInfoPlanet[2], auxInfoPlanet[3], auxInfoPlanet[4]);
	printf("\n");

	return 0;
}