# Sun, moon, and planet's (SMP) location calculator #

I wrote this code based on the excellent description written by Mr. Paul Schlyter which could be found at http://www.stjarnhimlen.se/comp/tutorial.html#9. The purpose of the code for me is to get the RA/DEC of SMP, so I could command my self-made telescope mount to point at the object with an accuracy of arc minutes (quite possibly not that good, probably within half a degree, especially for moon). Because of this reason, the accuracy may not be sufficient for precise astronomical application. Parallax could be compensated for moon, for other objects, I don't think it is necessary for the accuracy. But it is not difficult to implement it, as already described in the excellent tutorial mentioned above. 

The usage of the code is quite simple, in main(), there are examples of how to execute functions to get RA/DECs (in unit of degrees) for each objects of SMP. One could adapt these functions to one's needs.

Only standard C math library is needed for compiling.

Checked a few dates and a number of locations against results from here: https://www.heavens-above.com/moon.aspx?lat=39.9042&lng=116.4074&loc=北京市&alt=0&tz=UCT

I encourage you to try it aginast results from the aforementioned website.

Lu Feng
2019/12/27

